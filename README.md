# hooktftpd

Hooktftp is a dynamic read-only TFTP server. It's dynamic in a sense it is
executes hooks matched on read requests (RRQ) instead of reading files from
the file system. Hooks are matched with regular expressions and on match
hooktftp will execute a script, issue a HTTP GET request or just reads the file
from the filesystem.

This is inspired by [puavo-tftp]. It's written in Go in the hope of being faster
and more stable.

This is fork with additional hooks planned: mongodb/gridfs and openstack swift
It is part on DOCSIS provisioning system at early stage.

## Usage

    hooktftpd [-v] [config]

Config will be read from `/etc/hooktftp.yml` by default. Verbose option `-v`
print log to stderr insteadof syslog.

## Configuration

Configuration file is in json format and it can contain following keys:

  - `port`: Port to listen (required)
  - `user`: User to drop privileges to.
	**WARN: Currently feature not supported due to Linux go implementation syscalls
	removal (Issue: https://github.com/golang/go/issues/1435)**
	Recomended solution: Leave 'user' empty and set binary privileges to bind < 1024 port:
	sudo setcap CAP_NET_BIND_SERVICE+ep hooktftpd

  - `hooks`: Array of hooks. One or more is required

### Hooks

Hooks consists of following keys:

  - `type`: Type of the hook
    - `file`: Get response from the file system
    - `http`: Get response from a HTTP server
    - `shell`: Get response from external application
  - `regexp`: Regexp matcher
    - Hook is executed when this regexp matches the requested path
  - `template`: A template where the regexp is expanded

Regexp can be expanded to the template using the `$0`, `$1`, `$1` etc.
variables. `$0` is the full matched regexp and rest are the matched regexp
groups.

### Example

To share files from `/srv/tftp` add following hook to "hooks" json section:

```json
   {
	"type": "file",
	"regexp":"^.*$",
	"template": "/srv/tftp/$0"
   }

```

Share custom boot configurations for PXELINUX from a custom http server 
add following option to "hooks" section:

```json
    {
	"type": "http",
    "regexp": "(.*)$",
    "template": "http://localhost/$1"
	}
```

The order of the hooks matter. The first one matched is used.

To put it all together:

```json
{
"port": "69",
"user": "",
"hooks": [
        {"type": "file",
        "regexp":"^.*$",
        "template": "/srv/tftp/$0"
        },
        {"type": "http",
        "regexp": "(.*)$",
        "template": "http://localhost/$1"
        }
        ]
}
```

## Install

### Compiling from sources

[Install Go][] (1.3 or later), make sure you have git and bazaar too. 
Assuming you've successfully set up GOPATH and have GOPATH/bin on your path, simply:
    
    cd hooktftpd; go install
    
Now you should have a standalone hooktftp binary on your path.

    hooktftpd -h
    Usage: hooktftpd [-v] [config]

### Build Debian package

The package has been created with devscripts and dh-make. To build it:

    debuild -e GOPATH=$PWD -us -uc

## History

Please read debian changelog file.

## Tests

You can start unit and end-to-end test with Makefile target `test`:

    make test


[epeli/hooktftp]: https://github.com/epeli/hooktftp
[puavo-tftp]: https://github.com/opinsys/puavo-tftp
[Install Go]: http://golang.org/doc/install
