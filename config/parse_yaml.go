package config

import (
	"launchpad.net/goyaml"
)

func ParseYaml(yaml []byte) (*Config, error) {
	var config Config
	err := goyaml.Unmarshal(yaml, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
