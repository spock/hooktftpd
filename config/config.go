package config

type HookDef struct {
	Description string "description"
	Type        string "type"
	Regexp      string "regexp"
	Template    string "template"
}

type Config struct {
	Port     string    "port"
	Host     string    "host"
	User     string    "user"
	HookDefs []HookDef `json:"hooks"`
}

func (d *HookDef) GetType() string {
	return d.Type
}

func (d *HookDef) GetTemplate() string {
	return d.Template
}

func (d *HookDef) GetDescription() string {
	return d.Description
}

func (d *HookDef) GetRegexp() string {
	return d.Regexp
}
